# Changelog
All notable changes to MLFlowCollab will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
Among others adding methods for other machine learning packages to be easily logged.

## [0.0.4] - 2022-01-26
### Added
- Functionality to use different score metrics for sklearn logging

## [0.0.3] - 2021-12-20
### Added 
- Log file(s) and/or folder(s).
- Pass run name to run.

### Changed
- Log model file is now optional.

### Fixed
- Version number didn't load correctly in `__init__.py`. It does now.

## [0.0.2] - 2021-12-06
### Added
- Version number in 'mlflowcollab/_version.py`
- CHANGELOG.md

### Changed
- Get version number from module instead of `setup.py`

## [0.0.1] - 2021-11-19

Initial commits: first working versions


[0.0.2]: https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/mlflowcollab/-/merge_requests/6
[0.0.1]: https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/mlflowcollab/-/commit/c1d62ded173fbbe29e370a610320b43b659d5ccb
Contributing to MLFlowCollab
============================

In what ways can I contribute?
------------------------------
There are several ways to contribute to MLFlowCollab:
1. Propose an enhancement to the package or examples
2. Test the package and propose bugfixes or improvements
3. Fix bugs
4. Execute enhancements/improvements
5. Improve documentation (from typos to additional text)
6. Like issues that should be fixed according to you and dislike the ones to which you disagree
7. Let me know that you use the package! What do you think of it in general? Or star the package! Tell your friends and colleagues about it, etc.

How to contribute?
------------------
-  (1.) and (2.): Create an [issue](https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/mlflowcollab/-/issues/new?issue) (if no similar issue already exists), give it the right label and if you know someone who can execute this, also assign it to someone.
- (3.), (4.) and (5.): Clone the repository, make a new branch with a clear name, complete the improvements/bugfixes/documentation etc., if any code is written, also write a docstring, if useful, also add addition to the example, execute the unittest and see if all tests succeed (not available right now), change the version of the package in the [version file](mlflowcollab/_version.py) according to [semantic versioning](https://semver.org/), add an explanation in [CHANGELOG.md](CHANGELOG.md) in line with [keepachangelog.com](https://keepachangelog.com/). Then [tag your final commit](https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/mlflowcollab/-/tags/new) and [create a merge request](https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/mlflowcollab/-/merge_requests/new). At least someone should review the code (this cannot be the same person as the writer) and finally, only Sjoerd Gnodde has access to complete the merge request.



Code of Conduct
---------------

We abide by the principles of openness, respect, and consideration of others
of the Python Software Foundation: https://www.python.org/psf/codeofconduct/.